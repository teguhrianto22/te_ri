import * as OfflinePluginRuntime from 'offline-plugin/runtime';

import './index.html';
import './theme.scss';
import Tabs from './assets/js/components/tabs';
import Profile from './assets/js/profile';
import Upload from './assets/js/upload';

const tabs = new Tabs();
const profile = new Profile();
const upload = new Upload();

OfflinePluginRuntime.install();
