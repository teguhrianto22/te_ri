class Tabs {
  constructor() {
    this.renderTab();
  }

  // eslint-disable-next-line class-methods-use-this
  renderTab() {
    // Get the tabbed nav
    const componentNav = document.querySelector('[role="tablist"]');

    // Get all the tabs
    const tabs = Array.from(componentNav.querySelectorAll('.nav__item__link'));

    // Get all the content panels
    const contents = Array.from(document.querySelectorAll('.tab-content__panel'));

    // Loop through the tabs
    tabs.forEach(tab => {
      // Add event listener
      tab.addEventListener('click', () => {
        // Match the tab to the appropriate content panel by searching for content panel ID that matches the href value
        const { target } = tab.dataset;
        const tabContent = document.querySelector(target);

        // Remove '--active' class from all tabs
        tabs.forEach(tabItem => tabItem.classList.remove('nav__item__link--active'));

        // Remove '--active' class from all content panels
        contents.forEach(contentItem => contentItem.classList.remove('tab-content__panel--active'));

        // Add '--active' to current tab
        tab.classList.add('nav__item__link--active');

        // Add '--active' class to current content panel
        tabContent.classList.add('tab-content__panel--active');
      });
    });
  }
}

export default Tabs;
