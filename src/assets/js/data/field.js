const dataField = {
  name: Array.from(document.querySelectorAll('[data-field="name"]')),
  avatar: document.querySelector('[data-field="avatar"]'),
  cover: document.querySelector('[data-field="cover"]'),
  phone: Array.from(document.querySelectorAll('[data-field="phone"]')),
  address: Array.from(document.querySelectorAll('[data-field="address"]')),
  ratings: Array.from(document.querySelectorAll('[data-field="ratings"]')),
  reviews: Array.from(document.querySelectorAll('[data-field="reviews"]')),
  followers: Array.from(document.querySelectorAll('[data-field="followers"]')),
  website: Array.from(document.querySelectorAll('[data-field="website"]'))
};

export default dataField;
