const profileData = [
  {
    id: '1',
    name_first: 'Jessica',
    name_last: 'Parker',
    avatar: require('../../img/avatar.jpg'),
    cover: require('../../img/bg-default.png'),
    phone: '(949) 325 - 68594',
    address: 'Newport Beach, CA',
    ratings: '4',
    reviews: '6',
    followers: '15',
    website: 'www.seller.com'
  }
];
export default profileData;
