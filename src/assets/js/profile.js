/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
import profileData from './data/profile';
import dataField from './data/field';

class Profile {
  constructor() {
    // Generated Popup
    this.generatedPopup();

    // Render Data
    this.render();

    // Popup Actions
    this.popupActions();

    // Actions
    document.addEventListener('click', e => {
      if (e.target.classList.contains('btn-save-desktop')) {
        e.preventDefault();
        const profileFormDesktop = document.querySelector('.profile--desktop');
        this.updateData(profileFormDesktop);
      }

      if (e.target.classList.contains('btn-save-mobile')) {
        e.preventDefault();
        const profileFormMobile = document.querySelector('.profile--mobile');
        this.updateData(profileFormMobile);
      }
      //Switch edit
      if (e.target.classList.contains('btn-edit-mobile')) {
        document.querySelector('#about').classList.toggle('open-form-mobile');
      }
    });
  }

  render() {
    profileData.map(profile => {
      dataField.avatar.setAttribute('src', profile.avatar);
      dataField.cover.setAttribute('src', profile.cover);
      dataField.name.map(name => {
        name.textContent = `${profile.name_first} ${profile.name_last}`;
      });
      dataField.phone.map(phone => {
        phone.textContent = profile.phone;
      });
      dataField.address.map(address => {
        address.textContent = profile.address;
      });
      dataField.reviews.map(reviews => {
        reviews.textContent = profile.reviews;
      });
      dataField.followers.map(followers => {
        followers.textContent = profile.followers;
      });
      dataField.website.map(website => {
        website.textContent = profile.website;
      });

      const inputField = {
        nameFirst: Array.from(document.querySelectorAll('[name="name_first"]')),
        nameLast: Array.from(document.querySelectorAll('[name="name_last"]')),
        avatar: document.querySelector('[name="avatar"]'),
        cover: document.querySelector('[name="cover"]'),
        phone: Array.from(document.querySelectorAll('[name="phone"]')),
        address: Array.from(document.querySelectorAll('[name="address"]')),
        website: Array.from(document.querySelectorAll('[name="website"]'))
      };

      // Input
      // inputField.avatar.value = profile.avatar;
      // inputField.cover.value = profile.cover;
      // console.log(inputField.name);
      inputField.nameFirst.map(inputName => {
        inputName.value = profile.name_first;
      });
      inputField.nameLast.map(inputName => {
        inputName.value = profile.name_last;
      });
      inputField.phone.map(inputPhone => {
        inputPhone.value = profile.phone;
      });
      inputField.address.map(inputAddress => {
        inputAddress.value = profile.address;
      });
      inputField.website.map(inputWebsite => {
        inputWebsite.value = profile.website;
      });
    });
  }

  generatedPopup() {
    this.templatePopup = document.querySelector('[data-role="form-popup"]').innerHTML;
    this.profilePopup = Array.from(document.querySelectorAll('.profile__about__item__action'));
    this.profilePopup.forEach((popup, index) => {
      popup.insertAdjacentHTML('afterbegin', this.templatePopup);
      const inputPopup = popup.querySelector('input');
      const inputLabel = popup.querySelector('label');

      if (index == 0) {
        inputPopup.name = 'name_first';
        inputLabel.textContent = 'First Name';
        // Generate Form Lastname in popup
        const nameLast = popup.querySelector('.form-group');
        const clonedInput = nameLast.cloneNode(true);
        clonedInput.querySelector('input').name = 'name_last';
        clonedInput.querySelector('label').textContent = 'Last Name';
        popup
          .querySelector('.card__body .form-group')
          .insertAdjacentElement('afterend', clonedInput);
      } else if (index == 1) {
        inputPopup.name = 'website';
        inputLabel.textContent = 'Website';
      } else if (index == 2) {
        inputPopup.name = 'phone';
        inputLabel.textContent = 'Phone Number';
      } else {
        inputPopup.name = 'address';
        inputLabel.textContent = 'City, State & ZIP';
      }
    });
  }

  popupActions() {
    const profileAbout = document.querySelector('.profile__about');
    const pencils = Array.from(profileAbout.querySelectorAll('[data-action="edit"]'));
    const pencilCancel = profileAbout.querySelectorAll('[data-action="cancel-edit"]');

    pencils.forEach(pencil => {
      // Add event listener
      pencil.addEventListener('click', () => {
        const parentOpen = pencil.parentElement;
        pencils.forEach(pencilItem =>
          pencilItem.parentElement.classList.remove('profile__about__item__action--open')
        );
        parentOpen.classList.add('profile__about__item__action--open');
      });
    });

    pencilCancel.forEach(cancel => {
      cancel.addEventListener('click', e => {
        e.preventDefault();
        pencils.forEach(pencilItem =>
          pencilItem.parentElement.classList.remove('profile__about__item__action--open')
        );
      });
    });
  }

  updateData(selector) {
    const inputFieldValue = {
      nameFirst: selector.querySelector('[name="name_first"]').value,
      nameLast: selector.querySelector('[name="name_last"]').value,
      phone: selector.querySelector('[name="phone"]').value,
      address: selector.querySelector('[name="address"]').value,
      website: selector.querySelector('[name="website"]').value
    };

    profileData.map(profile => {
      profile.name_first = inputFieldValue.nameFirst;
      profile.name_last = inputFieldValue.nameLast;
      profile.phone = inputFieldValue.phone;
      profile.address = inputFieldValue.address;
      profile.website = inputFieldValue.website;

      return profile;
    });

    this.render();
  }
}

export default Profile;
