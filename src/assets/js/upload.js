class Upload {
  constructor() {
    // Upload Trigger
    document.addEventListener('click', e => {
      if (e.target.classList.contains('btn-upload-cover')) {
        const fileCover = document.querySelector('[name="cover"]');
        const imgCover = document.querySelector('[data-field="cover"]');
        this.uploadImage(fileCover, imgCover);
      }

      if (e.target.classList.contains('btn-upload-avatar')) {
        const fileAvatar = document.querySelector('[name="avatar"]');
        const imgAvatar = document.querySelector('[data-field="avatar"]');
        this.uploadImage(fileAvatar, imgAvatar);
      }
    });
  }

  uploadImage(trigger, image) {
    trigger.click();
    trigger.addEventListener('change', event => {
      const input = event.target;
      // Ensure that you have a file before attempting to read it
      if (input.files && input.files[0]) {
        const reader = new FileReader();

        reader.onload = function(e) {
          // get loaded data and render thumbnail.
          image.src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(input.files[0]);
      }
    });
  }
}

export default Upload;
